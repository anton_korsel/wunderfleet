## WunderFleet Backend Assigment

### Possible performance optimizations

1. We can keep unregistered users in
 in-memory database like Redis, so
 it will automatically delete very old
 registrations and it will be more faster
 than SQL

2. We can use cache for HTML or templates

### Things could be done better

1. We can use redirect mechanism between registration
 steps, so it will allow us to unlock F5 in
 browser (currently pressing F5 will send POST
 data to server again)

2. We can add error handling to all steps (validator)

3. We can add routing, it will help us to use
 browser history and to separate /registration
 page from index page

### How to run

1. Start Docker with `docker-compose up`
2. After Docker is on, open in browser
 `localhost:8080`
3. Unit tests also run in Docker with
 `docker_tests.cmd`