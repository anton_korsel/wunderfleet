<?php

require_once '_loader.php';

DatabaseHelper::initDb(); // be sure we have all tables

function getRegistrationPresenter()
{
    $customer = new Customer();
    $view = new HeaderAndFooterView();

    return new RegistrationPresenter($customer, $view);
}


function router()
{
    $presenter = getRegistrationPresenter();

    $cancel_registration = (int)($_POST['cancel_registration'] ?? 0);
    if ($cancel_registration === 1) {
        return $presenter->cancelRegistration();
    }

    $start_registration = (int)($_POST['start_registration'] ?? 0);
    if ($start_registration === 1) {
        return $presenter->startRegistration();
    }

    $finish_registration = (int)($_POST['finish_registration'] ?? 0);
    if ($finish_registration === 1) {
        return $presenter->finishRegistration();
    }

    return $presenter->continueRegistration();
}


echo router();