<?php

class Model
{
    protected $db;


    public function __construct()
    {
        $this->db = Database::getInstance();
    }


    public function prepareUpdateSql($table, $data, $id, $fields = []): string
    {
        $result = 'UPDATE ' . $table . ' SET ';
        $field_sets = '';
        if (empty($fields)) {
            $fields = array_keys($data);
        }
        foreach ($fields as $key => $field) {
            if (isset($data[$field])) {
                $data_value = $data[$field];
                if (!empty($field_sets)) {
                    $field_sets .= ',';
                }
                $field_sets .= $field . '=';
                if (is_numeric($data_value)) {
                    $field_sets .= $data_value;
                } else {
                    $field_sets .= "'" . $data_value . "'";
                }
            }
        }
        $result .= $field_sets . ' WHERE id=' . $id;

        return !empty($field_sets) ? $result : '';
    }
}