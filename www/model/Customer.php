<?php

require_once 'Model.php';

class Customer extends Model
{
    private const CUSTOMER_TABLE = 'customers';


    public function createCustomer(): void
    {
        $sql = 'INSERT INTO ' . self::CUSTOMER_TABLE . ' (first_name) VALUES (NULL)';
        $customer_id = $this->db->insert($sql);
        Session::setCustomerId($customer_id);
    }


    public function removeCustomer($customer_id): void
    {
        $this->db->query('DELETE FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id=' . $customer_id . ' AND registration_stage < 4'); // do not remove complete registration
    }


    public function getRegistrationStep($customer_id): int
    {
        $step = $this->db->fetchFirst(
            'SELECT registration_stage FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id = ' . $customer_id);

        return $step['registration_stage'] ?: 0;
    }


    public function setRegistrationStage($stage): void
    {
        $this->updateCustomer(['registration_stage' => $stage]);
    }


    public function updateCustomer($data): void
    {
        $customer_id = Session::getCustomerId();

        if (!empty($customer_id)) {
            $sql = $this->prepareUpdateSql(self::CUSTOMER_TABLE, $data, $customer_id);
            if (!empty($sql)) {
                $this->db->query($sql);
            }
        }
    }


    public function getStep1Data($customer_id): array
    {
        $customer = $this->db->fetchFirst(
            'SELECT first_name, last_name, phone FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id = ' . $customer_id);

        return $customer ?: [
            'first_name' => '',
            'last_name' => '',
            'phone' => ''
        ];
    }


    public function getStep2Data($customer_id): array
    {
        $customer = $this->db->fetchFirst(
            'SELECT zip_code, city, address, house FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id = ' . $customer_id);

        return $customer ?: [
            'zip_code' => '',
            'city' => '',
            'house' => '',
            'address' => ''
        ];
    }


    public function getStep3Data($customer_id): array
    {
        $customer = $this->db->fetchFirst(
            'SELECT account_owner, iban FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id = ' . $customer_id);

        return $customer ?: [
            'account_owner' => '',
            'iban' => ''
        ];
    }


    public function getStep4Data($customer_id): array
    {
        $customer = $this->db->fetchFirst(
            'SELECT payment_data_id FROM ' . self::CUSTOMER_TABLE .
            ' WHERE id = ' . $customer_id);

        return $customer ?: [
            'payment_data_id' => ''
        ];
    }
}