function nextPage() {
    let form = document.getElementById('registration_form');
    form.submit();
    return false;
}

function cancelRegistration() {
    let form = document.getElementById('cancel_registration_form');
    form.submit();
    return false;
}