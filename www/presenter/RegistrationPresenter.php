<?php


class RegistrationPresenter
{
    private $customer;
    private $view;


    public function __construct(Customer $customer, HeaderAndFooterView $view)
    {
        $this->customer = $customer;
        $this->view = $view;
    }


    public function continueRegistration(): string
    {
        $customer_id = Session::getCustomerId();
        $step = $this->customer->getRegistrationStep($customer_id);

        if (empty($step)) {
            return $this->index();
        }

        if ($step > 0) {
            switch ($step) {
                case 1:
                    return $this->step1();
                case 2:
                    return $this->step2();
                case 3:
                    return $this->step3();
                case 4:
                    return $this->step4();
            }
        }

        return $this->p404();
    }


    public function cancelRegistration(): string
    {
        $customer_id = Session::getCustomerId();
        if ($customer_id !== 0) {
            $this->customer->removeCustomer($customer_id);
            Session::unsetCustomerId();
        }

        return $this->index();
    }


    public function startRegistration(): string
    {
        $customer_id = Session::getCustomerId();
        if (empty($customer_id)) {
            $this->customer->createCustomer();
        }

        return $this->step1();
    }


    public function finishRegistration(): string
    {
        Session::unsetCustomerId();

        return $this->index();
    }


    public function index(): string
    {
        $data['title'] = 'Index Page';
        return $this->view->render('index.html', $data);
    }


    public function step1(): string
    {
        if (!$this->saveStep1Data()) {
            $data = $this->customer->getStep1Data(Session::getCustomerId());

            $data['title'] = 'Registration Step 1';

            return $this->view->render('step1.html', $data);
        }

        $this->customer->setRegistrationStage(2);

        return $this->step2();
    }


    private function saveStep1Data(): bool
    {
        $first_name = $_POST['first_name'] ?? null;
        $last_name = $_POST['last_name'] ?? null;
        $phone = $_POST['phone'] ?? null;

        $this->customer->updateCustomer([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'phone' => $phone
        ]);

        return !empty($first_name) && !empty($last_name) && !empty($phone);
    }


    public function step2(): string
    {
        if (!$this->saveStep2Data()) {
            $data = $this->customer->getStep2Data(Session::getCustomerId());
            $data['title'] = 'Registration Step 2';
            return $this->view->render('step2.html', $data);
        }

        $this->customer->setRegistrationStage(3);

        return $this->step3();
    }


    private function saveStep2Data(): bool
    {
        $city = $_POST['city'] ?? null;
        $zip_code = $_POST['zip_code'] ?? null;
        $address = $_POST['address'] ?? null;
        $house = $_POST['house'] ?? null;

        $this->customer->updateCustomer([
            'city' => $city,
            'zip_code' => $zip_code,
            'house' => $house,
            'address' => $address
        ]);

        return !empty($city) && !empty($zip_code) && !empty($address) && !empty($house);
    }


    public function step3(): string
    {
        if (!$this->saveStep3Data()) {
            $data = $this->customer->getStep3Data(Session::getCustomerId());
            $data['title'] = 'Registration Step 3';
            return $this->view->render('step3.html', $data);
        }

        $register_result = $this->registerOnForeignSite();
        if ($register_result === true) {
            $this->customer->setRegistrationStage(4);

            return $this->step4();
        }

        return $this->error($register_result);
    }


    private function registerOnForeignSite()
    {
        try {
            $customer_id = Session::getCustomerId();
            $payment_info = $this->customer->getStep3Data($customer_id);

            $payment_data = RestClient::postJson(RestClient::OTHER_SERVER, [
                'customerId' => $customer_id,
                'owner' => $payment_info['account_owner'],
                'iban' => $payment_info['iban']
            ]);

            $decoded = RestClient::decodeJson($payment_data);
            if (is_array($decoded) && array_key_exists('paymentDataId', $decoded)) {
                $paymentDataId = $decoded['paymentDataId'];
                $this->customer->updateCustomer(['payment_data_id' => $paymentDataId]);
                return true;
            }

            return 'Registration error: ' . print_r($decoded, true);
        } catch (Exception $e) {
            return 'Registration error: ' . $e->getMessage();
        }
    }


    private function saveStep3Data(): bool
    {
        $owner = $_POST['account_owner'] ?? null;
        $iban = $_POST['iban'] ?? null;

        $this->customer->updateCustomer([
            'account_owner' => $owner,
            'iban' => $iban
        ]);

        return !empty($owner) && !empty($iban);
    }


    public function step4(): string
    {
        $data = $this->customer->getStep4Data(Session::getCustomerId());
        $data['title'] = 'Registration Step 4';
        return $this->view->render('step4.html', $data);
    }


    public function p404(): string
    {
        $data['title'] = 'Page not found';
        return $this->view->render('404.html', $data);
    }


    public function error($message): string
    {
        return $this->view->render('error.html', [
            'error' => $message,
            'title' => 'Error'
        ]);
    }
}