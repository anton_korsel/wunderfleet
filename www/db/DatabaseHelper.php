<?php

require_once 'Database.php';

class DatabaseHelper
{
    public static function tableExists($table_name): bool
    {
        $db = Database::getInstance();
        if (isset($db)) {
            $sql = "select count(*) from $table_name limit 1";
            $table_exists = $db->getResult($sql);
            return (bool)$table_exists;
        }

        return false;
    }


    public static function initDb(): Database
    {
        $db = Database::getInstance();
        if (isset($db) && !self::tableExists('customers')) {
            $sql = file_get_contents('/var/www/html/sql/init.sql');
            $db->query($sql);
        }
        return $db;
    }
}