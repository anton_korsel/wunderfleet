<?php

require_once 'DbSettings.php';

class Database
{
    private $mysql;

    private static $db;


    public static function getInstance(): Database
    {
        if (self::$db === null) {
            self::$db = new Database();
        }
        return self::$db;
    }


    /**
     * Database constructor.
     */
    private function __construct()
    {
        $this->mysql = mysqli_connect(
            DbSettings::DB_HOST,
            DbSettings::DB_USER,
            DbSettings::DB_PASS,
            DbSettings::DB_NAME,
            DbSettings::DB_PORT);

        if (!$this->mysql) {
            echo 'ERROR: Unable to connect to MySQL.' . PHP_EOL . '<br>';
            echo 'Debugging errno: ' . mysqli_connect_errno() . PHP_EOL . '<br>';
            echo 'Debugging error: ' . mysqli_connect_error() . PHP_EOL . '<br>';
            exit;
        }
    }


    public function __destruct()
    {
        mysqli_close($this->mysql);
    }


    public function query($sql)
    {
        return mysqli_query($this->mysql, $sql);
    }


    private function exec($sql)
    {
        $stmt = $this->mysql->prepare($sql);
        if ($stmt) {
            $stmt->execute();
            return $stmt;
        }

        return false;
    }


    public function getResult($sql)
    {
        $stmt = $this->exec($sql);
        if ($stmt) {
            return $stmt->get_result();
        }

        return false;
    }


    public function fetchFirst($sql)
    {
        $result = $this->getResult($sql);
        if ($result) {
            return $result->fetch_assoc();
        }

        return false;
    }


    public function insert($sql)
    {
        $this->query($sql);
        return mysqli_insert_id($this->mysql);
    }
}