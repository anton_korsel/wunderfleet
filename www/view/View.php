<?php


class View
{
    public function renderTemplate($template, $data = array())
    {
        $template_path = __DIR__ . '/../template/' . $template;
        if (file_exists($template_path)) {
            $contents = file_get_contents($template_path);
            if (!empty($contents) && count($data) > 0) {
                foreach ($data as $key => $value) {
                    $contents = str_replace('{{' . $key . '}}', $value, $contents);
                }
            }
        } else {
            $contents = '';
        }
        return $contents;
    }
}