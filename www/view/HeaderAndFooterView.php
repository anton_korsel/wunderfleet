<?php


require_once 'View.php';

class HeaderAndFooterView extends View
{
    public function render($template, $data = array()): string
    {
        $data['cancel_next_buttons'] = $this->renderTemplate('cancel_next_buttons.html', $data);
        $data['content'] = $this->renderTemplate($template, $data);

        return $this->renderTemplate('header_and_footer.html', $data);
    }
}