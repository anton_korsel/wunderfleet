<?php


class RestClient
{
    public const OTHER_SERVER = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    /**
     * @param $url
     * @param $data
     * @return string
     * @throws Exception
     */
    public static function postJson($url, $data): string
    {
        return self::post($url, $data, true);
    }


    public static function toJson($data): string
    {
        if (is_array($data)) {
            $data = json_encode($data, JSON_THROW_ON_ERROR, 512);
        }
        return $data;
    }


    public static function decodeJson($json)
    {
        return json_decode($json, true, 512, JSON_THROW_ON_ERROR);
    }


    /**
     * @param $url
     * @param $data
     * @param bool $as_json
     * @return string
     * @throws Exception
     */
    public static function post($url, $data, $as_json = false): string
    {
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $as_json ? self::toJson($data) : $data);
        if ($as_json) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        }

        //So that curl_exec returns the contents of the cURL; rather than echoing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //execute post
        $result = curl_exec($ch);

        //collect error
        $error_code = curl_errno($ch);
        if ($error_code) {
            $error_msg = curl_error($ch);
        }

        curl_close($ch);

        if (!empty($error_msg)) {
            throw new RuntimeException($error_msg, $error_code);
        }

        return $result;
    }
}