<?php


class Session
{
    private const SESSION_LIFETIME_SECONDS = 86400; // 1 day
    private const SESSION_CUSTOMER_ID_KEY = 'customer_id';

    private const SESSION_STARTED = TRUE;
    private const SESSION_NOT_STARTED = FALSE;

    private $sessionState = self::SESSION_NOT_STARTED;

    private static $instance;


    private function __construct()
    {
    }


    public static function restoreSession(): Session
    {
        $old_session_id = $_COOKIE['sid'] ?? null;
        if (!empty($old_session_id)) {
            session_id($old_session_id);
        }

        return self::getInstance();
    }


    public static function getInstance(): Session
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        self::$instance->startSession();

        return self::$instance;
    }


    public static function getCustomerId()
    {
        return self::getInstance()->get(self::SESSION_CUSTOMER_ID_KEY);
    }


    public static function setCustomerId($customer_id): void
    {
        self::getInstance()->set(self::SESSION_CUSTOMER_ID_KEY, $customer_id);
    }


    public static function unsetCustomerId(): void
    {
        self::getInstance()->unset(self::SESSION_CUSTOMER_ID_KEY);
    }


    public function startSession(): bool
    {
        if ($this->sessionState === self::SESSION_NOT_STARTED) {
            ini_set('session.gc_maxlifetime', self::SESSION_LIFETIME_SECONDS);
            ini_set('session.cookie_lifetime', self::SESSION_LIFETIME_SECONDS);

            $this->sessionState = session_start();

            setcookie('sid', session_id(), time() + self::SESSION_LIFETIME_SECONDS);
        }

        return $this->sessionState;
    }


    public function set($name, $value): void
    {
        $_SESSION[$name] = $value;
    }


    public function get($name)
    {
        return $_SESSION[$name] ?? null;
    }


    public function isset($name): bool
    {
        return isset($_SESSION[$name]);
    }


    public function unset($name): void
    {
        unset($_SESSION[$name]);
    }


    public function destroy(): bool
    {
        if ($this->sessionState === self::SESSION_STARTED) {
            $this->sessionState = !session_destroy();
            unset($_SESSION);

            return !$this->sessionState;
        }

        return FALSE;
    }
}