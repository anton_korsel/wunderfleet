<?php

require_once __DIR__ . '/session/Session.php';

Session::restoreSession();

require_once __DIR__ . '/db/DbSettings.php';
require_once __DIR__ . '/db/Database.php';
require_once __DIR__ . '/db/DatabaseHelper.php';

require_once __DIR__ . '/rest/RestClient.php';

require_once __DIR__ . '/model/Model.php';
require_once __DIR__ . '/model/Customer.php';

require_once __DIR__ . '/presenter/RegistrationPresenter.php';
require_once __DIR__ . '/view/HeaderAndFooterView.php';