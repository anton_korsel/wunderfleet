create table if not exists customers
(
    id                 int auto_increment,
    session_id         varchar(40)  null,
    first_name         varchar(50)  null,
    last_name          varchar(50)  null,
    phone              varchar(20)  null,
    address            varchar(255) null,
    house              varchar(10)  null,
    zip_code           varchar(10)  null,
    city               varchar(50)  null,
    account_owner      varchar(100) null,
    iban               varchar(34)  null,
    payment_data_id    varchar(100) null,
    registration_stage int          not null default 1,
    constraint customers_pk
        primary key (id)
);