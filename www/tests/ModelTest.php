<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../_loader.php';


class ModelTest extends TestCase
{
    /** @noinspection SqlResolve */
    public function testPrepareUpdateSql(): void
    {
        $data = [
            'a' => 1,
            'b' => 2,
            'c' => 3
        ];
        $model = new Model();

        self::assertEquals('',
            $model->prepareUpdateSql('a', $data, 1, ['d']));

        self::assertEquals('',
            $model->prepareUpdateSql('a', $data, 1, ['e']));

        self::assertEquals('UPDATE a SET b=2 WHERE id=1',
            $model->prepareUpdateSql('a', $data, 1, ['b']));

        self::assertEquals('UPDATE a SET b=2,c=3 WHERE id=2',
            $model->prepareUpdateSql('a', $data, 2, ['b', 'c']));

        self::assertEquals('UPDATE a SET a=1,b=2,c=3 WHERE id=3',
            $model->prepareUpdateSql('a', $data, 3, ['a', 'b', 'c']));

        self::assertEquals('UPDATE a SET a=1,b=2,c=3 WHERE id=3',
            $model->prepareUpdateSql('a', $data, 3));
    }
}