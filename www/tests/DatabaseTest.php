<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../_loader.php';


class DatabaseTest extends TestCase
{
    public const TEST_TABLE = 'test_table';
    public const DATA_COL = 'data';


    public static function rnd(): int
    {
        try {
            return random_int(1, 10);
        } catch (Exception $e) {
            return 0;
        }
    }


    private function createTemporaryTable(): void
    {
        self::assertEquals(false,
            DatabaseHelper::tableExists(self::TEST_TABLE),
            'Temporary table must be deleted before tests');

        $creation_sql = 'create temporary table if not exists ' .
            self::TEST_TABLE . ' (id int auto_increment primary key, ' . self::DATA_COL . ' int)';
        Database::getInstance()->query($creation_sql);

        self::assertEquals(true,
            DatabaseHelper::tableExists(self::TEST_TABLE),
            'Could not create temporary table for tests: ' . $creation_sql);
    }


    private function dropTemporaryTable(): void
    {
        self::assertEquals(true,
            DatabaseHelper::tableExists(self::TEST_TABLE),
            'Temporary table must exist after tests');

        Database::getInstance()->query('drop table if exists ' . self::TEST_TABLE);

        self::assertEquals(false,
            DatabaseHelper::tableExists(self::TEST_TABLE),
            'Failed drop temporary table for tests');
    }


    private function insertRandomToTemporaryTable($expected_count = 1): void
    {
        self::assertEquals(true, DatabaseHelper::tableExists(self::TEST_TABLE));

        Database::getInstance()->query('insert into ' . self::TEST_TABLE . ' (' . self::DATA_COL . ') values (' . self::rnd() . ')');

        $result = Database::getInstance()->fetchFirst('select count(*) as cnt from ' . self::TEST_TABLE);

        self::assertEquals($expected_count, $result['cnt']);
    }


    public function testGetInstance(): void
    {
        self::assertNotEmpty(Database::getInstance());
    }


    public function testTableExists(): void
    {
        // be sure to delete temporary table
        Database::getInstance()->query('drop table if exists ' . self::TEST_TABLE);

        $this->createTemporaryTable();
        $this->dropTemporaryTable();
    }


    public function testGetResult(): void
    {
        $this->createTemporaryTable();

        $result = Database::getInstance()->fetchFirst(
            'select count(1) as cnt from ' . self::TEST_TABLE . ' limit 1');
        self::assertIsArray($result);
        self::assertIsNumeric($result['cnt'], 'Failed empty select from ' . self::TEST_TABLE);

        $this->dropTemporaryTable();
    }


    public function testQuery(): void
    {
        $this->createTemporaryTable();

        $this->insertRandomToTemporaryTable(1);
        $this->insertRandomToTemporaryTable(2);
        $this->insertRandomToTemporaryTable(3);

        $result = Database::getInstance()->getResult('select * from ' . self::TEST_TABLE);
        self::assertIsIterable($result, print_r($result, true));
        self::assertCount(3, $result, print_r($result, true));

        $this->insertRandomToTemporaryTable(4);

        $result = Database::getInstance()->query('select * from ' . self::TEST_TABLE);
        self::assertIsIterable($result, print_r($result, true));
        self::assertCount(4, $result, print_r($result, true));

        $this->dropTemporaryTable();
    }


    private function insert()
    {
        return Database::getInstance()->insert('insert into ' . self::TEST_TABLE .
            ' (' . self::DATA_COL . ') values (' . self::rnd() . ')');
    }


    public function testInsert(): void
    {
        $this->createTemporaryTable();

        $id = $this->insert();
        self::assertIsNumeric($id);
        self::assertEquals(1, $id);

        $id = $this->insert();
        self::assertIsNumeric($id);
        self::assertEquals(2, $id);

        $this->dropTemporaryTable();
    }
}
