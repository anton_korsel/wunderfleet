<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../_loader.php';

class CurlTest extends TestCase
{
    private const TEST_DATA = ['customerId' => 1, 'iban' => 'DE8234', 'owner' => 'Max Mustermann'];


    public function testCurl(): void
    {
        $ch = curl_init();
        self::assertNotEmpty($ch);

        curl_close($ch);
    }


    public function testToJson(): void
    {
        $result = RestClient::toJson(['a' => 1]);
        self::assertIsString($result);
        self::assertEquals('{"a":1}', $result);

        $result = RestClient::toJson(self::TEST_DATA);
        self::assertIsString($result);
        self::assertEquals('{"customerId":1,"iban":"DE8234","owner":"Max Mustermann"}', $result);
    }


    public function testDecodeJson(): void
    {
        $result = RestClient::decodeJson('{"a":1}');
        self::assertIsArray($result);
        self::assertEquals(1, $result['a']);

        $result = RestClient::decodeJson('true'); // RFC 8259
        self::assertIsBool($result);
        self::assertTrue($result);

        $result = RestClient::decodeJson('false'); // RFC 8259
        self::assertIsBool($result);
        self::assertFalse($result);

        $result = RestClient::decodeJson('123'); // RFC 8259
        self::assertIsNumeric($result);
        self::assertEquals(123, $result);

        $result = RestClient::decodeJson('{"a":{"b":"c"}}');
        self::assertIsArray($result);
        self::assertArrayHasKey('a', $result);
        self::assertArrayHasKey('b', $result['a']);
        self::assertEquals('c', $result['a']['b']);

        $result = RestClient::decodeJson('{}');
        self::assertIsArray($result);
        self::assertCount(0, $result);
    }


    public function testDecodeJsonError1(): void
    {
        $this->expectException('JsonException');
        RestClient::decodeJson('');
    }


    public function testDecodeJsonError2(): void
    {
        $this->expectException('JsonException');
        RestClient::decodeJson('truck');
    }


    public function testDecodeJsonError3(): void
    {
        $this->expectException('JsonException');
        RestClient::decodeJson('{]');
    }


    public function testPost(): void
    {
        try {
            $result = RestClient::postJson(RestClient::OTHER_SERVER, self::TEST_DATA);
            self::assertIsString($result, print_r($result, true));

            $decoded = RestClient::decodeJson($result);
            self::assertIsArray($decoded);
            self::assertArrayHasKey('paymentDataId', $decoded);
        } catch (Exception $e) {
            $this->assertEquals(-666, $e->getCode());
            $this->assertEquals('Exception Message', $e->getMessage());
        }
    }
}
